package com.weather.app;

import android.app.Application;

import com.weather.app.dagger.component.DaggerWeatherAppComponent;
import com.weather.app.dagger.component.WeatherAppComponent;
import com.weather.app.dagger.module.ApplicationModule;

public class BaseApplication extends Application {

    protected WeatherAppComponent initComponent() {
        return DaggerWeatherAppComponent.builder().applicationModule(new ApplicationModule(this)).build();
    }
}
