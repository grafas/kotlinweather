package com.weather.app.extensions

import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.weather.app.WeatherApp
import com.weather.app.activity.BaseActivity
import com.weather.app.activity.MainActivity

fun Fragment.activity(): BaseActivity? {
    return getActivity() as BaseActivity
}

fun AppCompatActivity.application(): WeatherApp {
    return getApplication() as WeatherApp;
}