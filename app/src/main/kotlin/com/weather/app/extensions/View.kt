package com.weather.app.extensions

import android.view.View


fun View.gone() {
    setVisibility(View.GONE)
}

/**
 * Hides view in case *clause* resolves to true. Otherwise will be visible. Could be overloaded with different fuction.
 */
fun View.goneIf(clause: () -> Boolean, or: () -> Unit = { visible() }) {
    if (clause.invoke()) {
        gone()
    } else {
        or.invoke()
    }
}

fun View.visible() {
    setVisibility(View.VISIBLE)
}

fun View.invisible() {
    setVisibility(View.INVISIBLE)
}

public var android.widget.ImageView.imageResource: Int
    get() = 0
    set(v) = setImageResource(v)