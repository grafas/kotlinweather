package com.weather.app.fragment

import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.*
import com.weather.app.R
import com.weather.app.api.response.CurrentObservation
import com.weather.app.api.response.SunPhase
import com.weather.app.extensions.imageResource
import kotlinx.android.synthetic.fragment_current_conditions.*
import org.jetbrains.anko.imageBitmap
import org.jetbrains.anko.text
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import timber.log.Timber

class CurrentConditionsFragment : BaseFragment() {

    companion object {
        fun newInstance(): CurrentConditionsFragment {
            return CurrentConditionsFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_current_conditions, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        api.getSatelliteImage(buildQuery())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { response ->
                            current_sattelite.imageBitmap = BitmapFactory.decodeStream(response.getBody().`in`())
                        },
                        { error ->
                            Timber.e(error.getMessage(), error)
                        })


        api.getConditions("Vilnius")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { response -> populateViews(response.currentObservation, response.sunPhase) },
                        { error -> Timber.e(error, error.getMessage()) }
                )
    }

    fun populateViews(observation: CurrentObservation?, sunPhase: SunPhase?) {
        if (observation != null) {
            current_icon.imageResource = observation.iconRes()
            current_weather.text = observation.weather
            current_temp.text = "${observation.tempC}\u00B0 C"
            current_feels_like.text = "${observation.feelslikeC}\u00B0 C"
            current_humidity.text = observation.relativeHumidity
            current_wind.text = "${observation.windKph} Km/H"
            current_gusts.text = "${observation.windGustKph} Km/H"
        }

        if (sunPhase != null) {
            current_sunrise.text = sunPhase.sunRiseStr()
            current_sunset.text = sunPhase.sunSetStr()
        }
    }

    fun buildQuery(): Map<String, String> {
        return hashMapOf(Pair("lat", "54.6833"),
                Pair("lon", "25.2833"),
                Pair("radius", "400"),
                Pair("width", "800"),
                Pair("height", "800"),
                Pair("key", "sat_ir4"),
                Pair("timelabel", "0"),
                Pair("basemap", "1"),
                Pair("borders", "1"))
    }
}