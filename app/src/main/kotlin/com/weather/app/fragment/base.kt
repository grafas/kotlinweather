package com.weather.app.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import com.weather.app.api.WeatherApi
import com.weather.app.dagger.component.WeatherAppComponent
import com.weather.app.extensions.activity
import javax.inject.Inject
import kotlin.properties.Delegates

open class BaseFragment : Fragment() {

    var api: WeatherApi by Delegates.notNull()
        @Inject set

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        onInject(activity()?.component())
    }

    open fun onInject(component: WeatherAppComponent?) {
        component?.inject(this)
    }
}