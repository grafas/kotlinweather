package com.weather.app.fragment

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.*
import android.support.v4.view.ViewPager
import android.view.*
import com.weather.app.R
import kotlinx.android.synthetic.fragment_main.navigation_tabs
import kotlinx.android.synthetic.fragment_main.navigation_tabs_pager

class MainFragment : BaseFragment(), ViewPager.OnPageChangeListener {

    companion object {
        fun newInstance(): MainFragment {
            return MainFragment()
        }
    }

    var tabs: TabLayout? = null
    var adapter: TabAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super<BaseFragment>.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super<BaseFragment>.onViewCreated(view, savedInstanceState)


        val viewPager: ViewPager = navigation_tabs_pager
        tabs = navigation_tabs

        adapter = TabAdapter(getFragmentManager())
        viewPager.setAdapter(adapter)

        tabLayout().addTab(tabLayout().newTab().setText("Current"), true)
        tabLayout().addTab(tabLayout().newTab().setText("Hourly"))
        tabLayout().addTab(tabLayout().newTab().setText("Forecast"))

        tabLayout().setOnTabSelectedListener(TabLayout.ViewPagerOnTabSelectedListener(viewPager));
        viewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabs));
        viewPager.addOnPageChangeListener(this);
    }

    fun tabLayout(): TabLayout {
        return tabs!!
    }

    override fun onPageScrollStateChanged(state: Int) {
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
    }

    override fun onPageSelected(position: Int) {
    }

    public class TabAdapter(val fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

        override fun getItem(position: Int): Fragment? {
            when (position) {
                0 -> return CurrentConditionsFragment.newInstance()
                1 -> return HourlyForecastFragment.newInstance()
                2 -> return FutureForecastFragment.newInstance()
                else -> throw IllegalArgumentException("Unknown tab index: ${position}")
            }
        }

        override fun getCount(): Int {
            return 3
        }
    }
}