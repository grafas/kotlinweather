package com.weather.app.api.response

import com.weather.app.R
import timber.log.Timber

public data class ConditionsResponse(var currentObservation: CurrentObservation? = null, var sunPhase: SunPhase? = null) : BaseResponse() {


    fun observation(init: CurrentObservation.() -> Unit): CurrentObservation {
        val observation = CurrentObservation()
        observation.init()
        currentObservation = observation
        return observation
    }

    fun sun(init: SunPhase.() -> Unit): SunPhase {
        val sun = SunPhase()
        sun.init()
        sunPhase = sun
        return sun
    }
}

public data class CurrentObservation(var observationLocation: Location? = null, var weather: String = "", var tempC: Int = 0,
                                     var icon: String = "", var feelslikeC: Int = 0, var relativeHumidity: String = "",
                                     var windDegrees: Int = 0, var windKph: Int = 0, var windGustKph: Int = 0) {

    fun iconRes(): Int {
        when (icon) {
            "clear" -> return R.drawable.ic_weather_clear
            else -> {
                Timber.d("Could not find icon for name '${icon}'")
                return R.drawable.ic_weather_default
            }
        }
    }

    fun location(init: Location.() -> Unit): Location {
        val loc = Location()
        loc.init()
        observationLocation = loc
        return loc
    }
}

public data class Location(var city: String = "", var countryIso3166: String = "") {

}

public data class SunPhase(var sunrise: SunTime = SunTime(0, 0), var sunset: SunTime = SunTime(0, 0)) {

    fun sunRiseStr(): String {
        return "${sunrise.hour}:${sunrise.minute}"
    }

    fun sunSetStr(): String {
        return "${sunset.hour}:${sunset.minute}"
    }
}

public data class SunTime(val hour: Int, val minute: Int) {

}
