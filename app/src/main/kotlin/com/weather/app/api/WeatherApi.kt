package com.weather.app.api

import com.weather.app.api.response.ConditionsResponse
import retrofit.client.Response
import retrofit.http.*
import rx.Observable

public interface WeatherApi {

    GET("/conditions/astronomy/q/{query}.json")
    fun getConditions(Path("query") query: String): Observable<ConditionsResponse>

    GET("/satellite/q/image.png")
    fun getSatelliteImage(QueryMap filters: Map<String, String>): Observable<Response>
}