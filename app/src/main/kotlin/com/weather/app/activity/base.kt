package com.weather.app.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.weather.app.dagger.component.WeatherAppComponent
import com.weather.app.extensions.application

public open class BaseActivity : AppCompatActivity() {

    var component: WeatherAppComponent? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        component = buildComponent()
        onInject(component())
    }

    open fun onInject(c: WeatherAppComponent) {
        c.inject(this)
    }

    fun component(): WeatherAppComponent {
        return component!!
    }

    open fun buildComponent(): WeatherAppComponent {
        return application().component()
    }
}