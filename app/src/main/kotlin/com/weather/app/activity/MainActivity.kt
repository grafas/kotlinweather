package com.weather.app.activity

import android.os.Bundle
import com.weather.app.R
import com.weather.app.fragment.MainFragment

public class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        component().inject(this)

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.tab_navigation_container, MainFragment.newInstance())
                .commitAllowingStateLoss()
    }
}
