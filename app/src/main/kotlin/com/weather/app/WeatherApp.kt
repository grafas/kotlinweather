package com.weather.app

import com.weather.app.dagger.component.WeatherAppComponent
import timber.log.Timber

open class WeatherApp : BaseApplication() {

    private var component: WeatherAppComponent? = null

    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())

        component = initComponent()
        component().inject(this)
    }

    public fun component(): WeatherAppComponent {
        return component!!
    }
}