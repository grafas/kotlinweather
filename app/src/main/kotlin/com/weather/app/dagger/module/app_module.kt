package com.weather.app.dagger.module

import android.app.Application
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

Singleton
Module
public class ApplicationModule(app: Application) {

    val app: Application = app

    Provides
    fun provideApplication(): Application {
        return app;
    }
}