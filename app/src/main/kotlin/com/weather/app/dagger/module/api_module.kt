package com.weather.app.dagger.module

import android.app.Application
import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import com.weather.app.R
import com.weather.app.api.WeatherApi
import dagger.Module
import dagger.Provides
import retrofit.RestAdapter
import retrofit.converter.GsonConverter
import javax.inject.Named
import javax.inject.Singleton

Module
public class ApiModule {

    Named("API_KEY")
    Singleton
    Provides
    fun provideApiKey(app: Application): String {
        return app.getResources().getString(R.string.api_key)
    }

    Singleton
    Provides
    fun provideApi(Named("API_KEY") apiKey: String): WeatherApi {
        val gson = GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create()

        val adapter = RestAdapter.Builder()
                .setEndpoint("http://api.wunderground.com/api/${apiKey}")
                .setConverter(GsonConverter(gson))
                .setLogLevel(RestAdapter.LogLevel.BASIC)
                .build()

        return adapter.create(javaClass<WeatherApi>())
    }
}
