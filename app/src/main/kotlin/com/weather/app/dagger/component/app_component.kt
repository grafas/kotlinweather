package com.weather.app.dagger.component

import com.weather.app.WeatherApp
import com.weather.app.activity.BaseActivity
import com.weather.app.dagger.module.ApiModule
import com.weather.app.dagger.module.ApplicationModule
import com.weather.app.fragment.BaseFragment
import dagger.Component
import javax.inject.Singleton

Singleton
Component(modules = arrayOf(ApplicationModule::class, ApiModule::class))
public interface WeatherAppComponent {

    fun inject(app: WeatherApp)
    fun inject(activity: BaseActivity)
    fun inject(activity: BaseFragment)
}
