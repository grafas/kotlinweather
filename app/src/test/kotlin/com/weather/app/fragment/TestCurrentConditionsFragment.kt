package com.weather.app.fragment

import com.weather.app.api.WeatherApi
import com.weather.app.api.response.ConditionsResponse
import com.weather.app.api.response.SunTime
import com.weather.app.test.TestBase
import org.junit.runner.RunWith
import org.mockito.Matchers
import org.mockito.Matchers.anyString
import org.mockito.Mockito.*
import org.robolectric.RobolectricGradleTestRunner
import rx.Observable
import rx.Observable.just
import javax.inject.Inject
import kotlin.properties.Delegates
import org.junit.Test as test

RunWith(RobolectricGradleTestRunner::class)
public class TestCurrentConditionsFragment : TestBase() {

    var api: WeatherApi by Delegates.notNull()
        @Inject set

    override fun setUp() {
        super.setUp()
        activity?.getTestComponent()?.inject(this)
    }

    test fun testInit() {
        val response = buildResponse()
        `when`(api.getSatelliteImage(Matchers.anyMap() as Map<String, String>)).thenReturn(Observable.empty())
        `when`(api.getConditions(anyString())).thenReturn(just(response));
        val fragment = CurrentConditionsFragment.newInstance()
        setFragment(fragment)

        verify(api, times(1))!!.getConditions(anyString())
        verify(api, times(1))!!.getSatelliteImage(Matchers.anyMap() as Map<String, String>)
    }

    fun buildResponse(): ConditionsResponse {
        return response {
            observation {
                location {
                    city = "Vilnius"
                    countryIso3166 = "LT"
                }
                weather = "Windy"
                tempC = 21
                feelslikeC = 20
                relativeHumidity = "45 %"
                windKph = 3
                windGustKph = 7
            }

            sun {
                sunrise = SunTime(7, 0)
                sunset = SunTime(20, 40)
            }
        }
    }
}

fun response(init: ConditionsResponse.() -> Unit): ConditionsResponse {
    val conditionsResponse = ConditionsResponse()
    conditionsResponse.init()
    return conditionsResponse
}
