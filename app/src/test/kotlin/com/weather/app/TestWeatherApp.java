package com.weather.app;

import com.weather.app.dagger.DaggerTestWeatherAppComponent;
import com.weather.app.dagger.TestWeatherAppComponent;
import com.weather.app.dagger.component.WeatherAppComponent;
import com.weather.app.dagger.module.ApplicationModule;

public class TestWeatherApp extends WeatherApp {

    private TestWeatherAppComponent component;

    @Override
    protected WeatherAppComponent initComponent() {
        component = DaggerTestWeatherAppComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();

        return component;
    }

    public TestWeatherAppComponent getTestComponent() {
        return component;
    }
}