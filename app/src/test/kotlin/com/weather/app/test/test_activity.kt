package com.weather.app.test

import android.os.Bundle
import com.weather.app.R
import com.weather.app.TestWeatherApp
import com.weather.app.activity.BaseActivity
import com.weather.app.dagger.TestWeatherAppComponent
import com.weather.app.dagger.component.WeatherAppComponent
import com.weather.app.extensions.application
import com.weather.app.fragment.BaseFragment
import org.robolectric.Robolectric
import org.robolectric.util.ActivityController

public class TestActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun buildComponent(): WeatherAppComponent {
        return (application() as TestWeatherApp).getTestComponent()
    }

    override fun onInject(c: WeatherAppComponent) {
        super.onInject(c)
        getTestComponent().inject(this)
    }

    public fun getTestComponent(): TestWeatherAppComponent {
        return (application() as TestWeatherApp).getTestComponent()
    }

    public fun setFragment(fragment: BaseFragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.tab_navigation_container, fragment)
                .commitAllowingStateLoss()

        getFragmentManager().executePendingTransactions()
    }

    companion object {
        public fun newInstance(): ActivityController<TestActivity> {
            return Robolectric.buildActivity(javaClass<TestActivity>())
        }
    }
}