package com.weather.app.test

import android.support.v4.app.Fragment
import com.weather.app.R
import com.weather.app.fragment.BaseFragment
import org.junit.After
import org.junit.Before
import org.robolectric.util.ActivityController

/**
 * @author Martynas Jurkus
 * *
 * @since 2013-08-12
 */
public abstract class TestBase {
    var activity: TestActivity? = null
    var activityController: ActivityController<TestActivity>? = null

    Before
    throws(Exception::class)
    public open fun setUp() {
        activityController = TestActivity.newInstance().create().start().resume()
        activity = activityController!!.get()
    }

    After
    throws(Exception::class)
    public fun tearDown() {
        activityController?.destroy()
    }

    protected fun setFragment(fragment: Fragment) {
        if (activity != null) {
            activity!!.getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.tab_navigation_container, fragment)
                    .addToBackStack(null)
                    .commit()
            activity!!.getFragmentManager().executePendingTransactions()
        }
    }

    public class EmptyFragment : BaseFragment()
}
