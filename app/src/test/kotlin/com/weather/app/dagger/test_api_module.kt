package com.weather.app.dagger

import com.weather.app.api.WeatherApi
import dagger.Module
import dagger.Provides
import org.mockito.Mockito
import javax.inject.Singleton

Module
public class TestApiModule {

    Provides
    Singleton
    fun provideApi(): WeatherApi {
        return Mockito.mock(javaClass<WeatherApi>())
    }
}