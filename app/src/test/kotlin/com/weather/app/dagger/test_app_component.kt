package com.weather.app.dagger

import com.weather.app.dagger.component.WeatherAppComponent
import com.weather.app.dagger.module.ApplicationModule
import com.weather.app.fragment.TestCurrentConditionsFragment
import com.weather.app.test.TestActivity
import dagger.Component
import javax.inject.Singleton

Singleton
Component(modules = arrayOf(ApplicationModule::class, TestApiModule::class))
public interface TestWeatherAppComponent : WeatherAppComponent {

    fun inject(activity: TestActivity)
    fun inject(fragment: TestCurrentConditionsFragment)
}